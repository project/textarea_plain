<?php

namespace Drupal\textarea_plain;

use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class TextareaPlainManager.
 *
 * Provides implementation for different hooks in order to allow the usage of
 * "plain text" as unique text format.
 *
 * @see textarea_plain_field_widget_third_party_settings_form()
 * @see textarea_plain_field_widget_settings_summary_alter()
 * @see textarea_plain_field_widget_form_alter()
 */
class TextareaPlainManager {
  use StringTranslationTrait;

  /**
   * List of allowed WidgetInterface plugins.
   *
   * @var string[]
   */
  protected $allowedWidgets = [
    'string_textfield',
    'string_textarea',
    'text_textfield',
    'text_textarea',
    'text_textarea_with_summary',
  ];

  /**
   * Checks if the given $plugin is allowed to apply this functionality.
   *
   * @param \Drupal\Core\Field\WidgetInterface $plugin
   *   The WidgetInterface plugin.
   *
   * @return bool
   *   TRUE if the given $plugin is allowed to force the plain text format.
   */
  public function pluginIsAllowed(WidgetInterface $plugin) {
    return in_array($plugin->getPluginId(), $this->allowedWidgets);
  }

  /**
   * Retrieves the settings form to be used on the widget as Third party.
   *
   * @param \Drupal\Core\Field\WidgetInterface $plugin
   *   The WidgetInterface plugin.
   *
   * @return array
   *   The settings form.
   */
  public function getSettingsForm(WidgetInterface $plugin) : array {
    $element['plain_text_format_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use plain text format only.'),
      '#default_value' => $plugin->getThirdPartySetting('textarea_plain', 'plain_text_format_only'),
      '#description' => $this->t('Check if you want to use the plain text format only on this instance.'),
    ];

    return $element;
  }

  /**
   * Retrieves the summary information.
   *
   * @param \Drupal\Core\Field\WidgetInterface $plugin
   *   The WidgetInterface plugin.
   *
   * @return array
   *   The summary array.
   */
  public function getSettingsSummary(WidgetInterface $plugin) : array {
    $summary = [];
    $setting = $plugin->getThirdPartySetting('textarea_plain', 'plain_text_format_only');

    if (!empty($setting)) {
      $summary[] = $this->t('Only "plain text" text format is allowed');
    }

    return $summary;
  }

  /**
   * Alters the given $element to force the usage of the plain text text format.
   *
   * It is only applied if it is configured to do it.
   *
   * @param array $element
   *   The $element array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   FormStateInterface object.
   * @param array $context
   *   The context array.
   */
  public function alterWidgetForm(array &$element, FormStateInterface $form_state, array $context) {
    $third_party_settings = $context['widget']->getThirdPartySettings();

    $is_enabled = !empty($third_party_settings['textarea_plain']['plain_text_format_only']);
    if (!$is_enabled) {
      return;
    }

    // We force to use the text_plain text format only:
    $element['#allowed_formats'] = ['plain_text'];
    $element['#format'] = 'plain_text';
  }

}
